option(BUILD_THREAD_SAFETY "Build for thread safety analysis." OFF)

if(BUILD_THREAD_SAFETY)
    if(${CMAKE_CXX_COMPILER_ID} STREQUAL Clang)
        add_compile_options(-Wthread-safety)
    else()
        message(FATAL_ERROR "BUILD_THREAD_SAFETY not supported yet for ${CMAKE_CXX_COMPILER_ID}")
    endif()
endif()
