if(MSVC)
    add_compile_options(
        /EHsc
        /W4
        /WX
        /Za
        /Zc:__cplusplus
        /permissive-
    )
else()
    add_compile_options(
        -g 
        -O3 
        -fPIC
        -shared
        -Wold-style-cast
        -Wno-narrowing 
        #-Wwritable-strings
        #-stdlib=libc++
        -isystem
        -fno-strict-aliasing
        -Wall
        -W
        -Wcast-align
        -Wconversion
        #-undefined 
        #dynamic_lookup
        # -Werror
        -Wold-style-cast
        -Wextra
        -Wpedantic
        -Wshadow
        -Wsign-conversion
        $<$<COMPILE_LANGUAGE:CXX>:-Wctor-dtor-privacy>
        $<$<COMPILE_LANGUAGE:CXX>:-Wnon-virtual-dtor>
        $<$<COMPILE_LANGUAGE:CXX>:-Wold-style-cast>
        $<$<COMPILE_LANGUAGE:CXX>:-Woverloaded-virtual>
    )
endif()
