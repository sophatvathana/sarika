set(BUILD_COVERAGE "" CACHE STRING "Build for coverage analysis. Coverage report types: html, xml.")

if(BUILD_COVERAGE)
    if(MSVC)
        message(FATAL_ERROR "BUILD_COVERAGE not supported yet for MSVC")
    else()
        add_compile_options(--coverage)
    endif()
endif()
