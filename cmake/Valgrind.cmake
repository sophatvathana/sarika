set(VALGRIND_TOOL memcheck CACHE STRING "Valgrind tools: memcheck, cachegrind, callgrind, massif, helgrind, drd.")

set(VALGRIND_COMMAND_OPTIONS "--tool=${VALGRIND_TOOL}")
if(VALGRIND_TOOL STREQUAL memcheck)
    string(APPEND VALGRIND_COMMAND_OPTIONS " \
        --leak-check=yes \
        --show-reachable=yes \
        --num-callers=50 \
        --suppressions=${PROJECT_SOURCE_DIR}/valgrind.supp \
        --error-exitcode=1 \
        --read-inline-info=yes \
        --xtree-memory=full \
        --xtree-memory-file=test-reports/memcheck-xtmemory.kcg \
        --child-silent-after-fork=yes \
        --xml=yes \
        --xml-file=test-reports/valgrind.xml")
# cachegrind
elseif(VALGRIND_TOOL STREQUAL callgrind)
    string(APPEND VALGRIND_COMMAND_OPTIONS " \
        --dump-instr=yes \
        --collect-jumps=yes \
        --callgrind-out-file=test-reports/callgrind.out")
# helgrind --xtree-memory=full --xtree-memory-file=test-reports/helgrind-xtmemory.kcg
# drd
elseif(VALGRIND_TOOL STREQUAL massif)
    string(APPEND VALGRIND_COMMAND_OPTIONS " \
        --xtree-memory=full \
        --xtree-memory-file=test-reports/massif-xtmemory.kcg \
        --massif-out-file=test-reports/massif.out")
else()
    message(FATAL_ERROR "VALGRIND_TOOL=${VALGRIND_TOOL} not supported yet")
endif()
