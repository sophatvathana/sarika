option(BUILD_PROFILING "Build for performance analysis." OFF)

if(BUILD_PROFILING)
    if(MSVC)
        message(FATAL_ERROR "BUILD_PROFILING not supported yet for MSVC")
    else()
        add_compile_options(-pg)
    endif()
endif()
