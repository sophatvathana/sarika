find_program(GCOVR_COMMAND gcovr)
mark_as_advanced(GCOVR_COMMAND)
if(GCOVR_COMMAND)
    function(add_gcovr_command coverage_target)
        if(BUILD_COVERAGE STREQUAL html OR BUILD_COVERAGE STREQUAL xml)
            add_custom_command(TARGET ${coverage_target} POST_BUILD
                COMMAND ${CMAKE_COMMAND} -E make_directory test-reports
                COMMAND ${GCOVR_COMMAND} --${BUILD_COVERAGE}
                    $<$<STREQUAL:${BUILD_COVERAGE},html>:--html-details>
                    -r "${PROJECT_SOURCE_DIR}/src"
                    --object-directory="${PROJECT_BINARY_DIR}"
                    -o "${PROJECT_BINARY_DIR}/test-reports/coverage.${BUILD_COVERAGE}"
                COMMENT "Generating gcovr ${BUILD_COVERAGE} reports")
        elseif(NOT BUILD_COVERAGE STREQUAL "")
            message(FATAL_ERROR "BUILD_COVERAGE=${BUILD_COVERAGE} not supported yet")
        endif()
    endfunction()
    add_gcovr_command(ExperimentalCoverage)
    add_gcovr_command(ContinuousCoverage)
    add_gcovr_command(NightlyCoverage)
endif()
