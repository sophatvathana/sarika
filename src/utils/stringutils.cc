#include <string>
#include "../core.h"
#include <iostream>
#include "stringutils.h"
#include <cstdlib>
#include <sstream>
#include <vector>
#include <cstdio>
#include <memory>
#include <stdexcept>
#include <array>
#include <regex>
#include <algorithm>
#include "lrucache.h"
#include "file.h"

namespace utils {
std::string Trim(const std::string& str,
                 const std::string& whitespace) {
    const auto strBegin = str.find_first_not_of(whitespace);
    if (strBegin == std::string::npos)
        return ""; // no content

    const auto strEnd = str.find_last_not_of(whitespace);
    const auto strRange = strEnd - strBegin + 1;

    return str.substr(strBegin, strRange);
}
void WriteData(std::string &vec) {
    std::string filename = "/tmp/tmp.txt";
    FILE *pOut = fopen(filename.c_str(), "w");
    fprintf(pOut, "%s\n", vec.c_str());
    fclose(pOut);
}

std::string exec(const char* cmd) {
    std::array<char, 128> buffer;
    std::string result;
    std::shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
    if (!pipe) throw std::runtime_error("popen() failed!");
    while (!feof(pipe.get())) {
        if (fgets(buffer.data(), 128, pipe.get()) != nullptr)
            result += buffer.data();
    }
    return result;
}

template <class T, class A> 
T Join(const A &begin, const A &end, const T  &t) {
  T result;
  for (A it=begin;  it!=end; it++) {
    if (!result.empty())
      result.append(t);
    result.append(*it);
  }
  return result;
}

std::string Join(const std::vector<std::string>::iterator &begin, const std::vector<std::string>::iterator &end, const std::string &t){
    return Join<std::string, std::vector<std::string>::iterator>(begin, end, t);
}

const std::vector<std::string> Segementation(const std::string& s){
    lru11::Cache<std::string, std::vector<std::string>> cache(3,0);
    std::vector<std::string> words;
    try{
        words = cache.get("words");
    }catch(std::exception &e){
        std::vector<std::string> words_;
        const char * project_path = std::getenv("PROJECT_PATH");
        utils::File::ReadFile2Vect(words_, std::string(project_path)+"/data/words.txt");
        cache.insert("words", words_);
        words = words_;
    }
    std::cout << words << std::endl;
}

std::vector<std::string> WordTokenize(std::string string){
    std::regex rgx("\\@(\\w+)\\@");
    std::regex rgx_var("\\$(\\w+)\\$");
    std::regex rgx_unk("_unk_");
    std::smatch match;
    std::smatch match_var;
    std::smatch match_unk;
    std::string match_func = "ffunc";
    std::string match_unk_txt = "unk";
    std::string match_var_txt = "vvar";
    
    std::string::const_iterator func( string.cbegin() );
    if (std::regex_search(func, string.cend(), match, rgx)){
        std::cout << "match: " << match[0] << '\n';
        match_func = match[0];
        string = std::regex_replace(string, rgx, "ffunc", std::regex_constants::format_first_only);
    }
    
    std::string::const_iterator searchStart( string.cbegin() );
    std::vector<std::string> lvars;
    while(std::regex_search(searchStart, string.cend(), match_var, rgx_var)){
        std::cout << match_var[0] << std::endl;
        lvars.push_back(match_var[0]);
        searchStart = match_var[0].second;
    }
    string = std::regex_replace(string, rgx_var, match_var_txt);
    
    std::string::const_iterator unk( string.cbegin() );
    if (std::regex_search(unk, string.cend(), match_unk, rgx_unk)){
        string = std::regex_replace(string, rgx_unk, match_unk_txt);
    }
    
    std::cout << string << std::endl;
    
    WriteData(string);
    const char * project_path = std::getenv("PROJECT_PATH");
    std::string cmd = "cd "+std::string(project_path)+"/thirdparty/khmerseq/ && sh km-5tag-seg-test.sh ";
    cmd += "model/km-5tag-seg-model ";
    cmd += "/tmp/tmp.txt /tmp ";
    cmd += "&& cat /tmp/tmp.txt.w";
    std::string text = exec(cmd.c_str());
    // remove("/tmp/tmp.txt");
    std::istringstream iss(text);
    std::vector<std::string> result{
        std::istream_iterator<std::string>(iss), {
            
        }
    };
    std::cout << "after segmentation" << result <<std::endl;
    auto pos = std::find(result.begin(), result.end(), "ffunc") - result.begin();
    std::cout << pos << std::endl;
    if(pos < result.size()) {
        result.at(pos) = match_func;
    }
    for(int i = 0; i < result.size(); ++i){
        if(result[i].compare("unk") == 0){
            result[i] = "_unk_";
        }
    }
    for(int i = 0, j = 0; i < result.size(); ++i){
        if(result[i].compare("vvar") == 0){
            result[i] = lvars[j];
            ++j;
        }
    }
  
    return result;
}

void ReplaceStringReg(std::string & string) {
//     std::regex reg1("([`|'])");
//     string = std::regex_replace(string, reg1, "", std::regex_constants::format_first_only);
    string.erase(std::remove(string.begin(), string.end(), '`'), string.end());
}

const std::vector<std::string> Explode(const std::string& s, const char& c) {
    std::string buff{""};
    std::vector<std::string> v;

    for(auto n:s) {
        if(n != c) buff+=n; else
        if(n == c && buff != "") { v.push_back(buff); buff = ""; }
    }
    if(buff != "") v.push_back(buff);

    return v;
}
}
