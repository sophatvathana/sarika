#include "file.h"
#include <iostream>
#include <string>
#include <iostream>
#include <sys/types.h>
#include <dirent.h>
#include <fstream>

namespace utils {

File::File(FILENAME filePath) {
    std::cout << "wellcome" << filePath << std::endl;
    this->path =  filePath;
}

void File::save(std::string str) {
    std::ofstream outfile;
    outfile.open(path);
    std::cout<<str<<std::endl;
    outfile.close();
}
std::vector<std::string> File::ls() {
    DIR *dpdf;
    struct dirent *epdf;
    std::vector<std::string> res;
    dpdf = opendir("./");
    if (dpdf != NULL){
        
        while (true){
            epdf = readdir(dpdf);
            if (!epdf) {
                break;
            }
            std::string str = epdf->d_name;
            res.push_back(str);
        }
    }
    return res;
}

bool File::Isfile(std::string filename){
    std::ifstream f(filename.c_str());
    return f.good();
}

void File::ReadDirectory(stringvec& v) {
    DIR* dirp = opendir(path.c_str());
    struct dirent * dp;
    while ((dp = readdir(dirp)) != NULL) {
        v.push_back(dp->d_name);
    }
    closedir(dirp);
}

std::string File::pwd(){
    struct passwd *pw = getpwuid(getuid());
    const char *homedir = pw->pw_dir;
    std::string s = homedir;
    return s;
}


}
