#ifndef SARIKA_UTILS_STRING_H
#define SARIKA_UTILS_STRING_H

#include <string>
#include <iostream>
#include <vector>

namespace utils {


std::string Trim(const std::string& str,
                 const std::string& whitespace = " \t");
void WriteData(std::string &vec);
std::vector<std::string> WordTokenize(std::string string);
template <class T, class A> 
T Join(const A &begin, const A &end, const T  &t);
std::string Join(const std::vector<std::string>::iterator &begin, const std::vector<std::string>::iterator &end, const std::string  &t);
void ReplaceStringReg(std::string & string);
const std::vector<std::string> Explode(const std::string& s, const char& c);
const std::vector<std::string> Segementation(const std::string& s);

}

#endif