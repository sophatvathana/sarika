#ifndef SARIKA_UTILS_CONFIG_H
#define SARIKA_UTILS_CONFIG_H
#include <string>
#include <iostream>
#include <sstream>
#include <vector>
#include <fstream>
#include <map>
#include <algorithm>

namespace utils {
static std::stringstream m_buffer;
static void Parse(std::map<std::string, std::string> &m_cfgMap) {
    
    std::string line;
    std::istringstream s(m_buffer.str());
    std::string commentKey("#");

    while (std::getline(s, line)) {
        // Ignore empty and comment lines
        if (!line.empty() && line.compare(0, commentKey.length(), commentKey)) {
            // Split the line
            std::vector<std::string> seglist;
            std::string segment;
            std::istringstream ss(line);


            while (std::getline(ss, segment, '=')) {
                // Trim the white space
                std::stringstream trimmer;
                trimmer << segment;
                segment.clear();
                trimmer >> segment;

                std::transform(segment.begin(), segment.end(), segment.begin(), ::tolower);

                seglist.push_back(segment);
            }

            // Only 
            if (seglist.size() == 2) {
                m_cfgMap[seglist[0]] = seglist[1];
            }
        }
    }
}

static bool Init(std::map<std::string, std::string> &m_cfgMap, std::string filename) {
    if (filename.empty()) {
        return false;
    }

    std::ifstream fileIn(filename);
    if (!fileIn.is_open()) {
        return false;
    }

    m_buffer << fileIn.rdbuf();

    std::string m_cfgFilename = std::move(filename);
  
    Parse(m_cfgMap);
    return true;
}
}
#endif