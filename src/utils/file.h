#ifndef SARIKA_UTILS_FILE_H
#define SARIKA_UTILS_FILE_H
#include "utils/config.h"
#include <iostream>
#include <fstream>
#include <dirent.h>
#include <vector>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <string>

namespace utils{

typedef std::string FILENAME;
typedef std::vector<std::string> stringvec;

class File{
    
    public:
        File(FILENAME filePath);
        void save(std::string str);
        std::vector<std::string> ls();
        void ReadDirectory(stringvec& v);
        std::string pwd();
        bool Isfile(std::string filename);
        static void ReadFile2Vect(std::vector<std::string> &data, std::string file_dir){
            std::string line;
            std::ifstream myfile (file_dir);
            if (myfile.is_open()) {
                while ( myfile.good() ) {
                    getline (myfile,line);
                    data.push_back(line);
                }
                myfile.close();
            }
            else std::cout << "Unable to open file" << std::endl; 
        }
        
    private:
        std::string path;
        
};
}
#endif