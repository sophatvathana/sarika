#include "preprocessing.h"
#include "utils/file.h"
#include <iostream>
#include "core.h"
#include "utils/stringutils.h"
#include <stdio.h>
#include <string>
#include <algorithm>

namespace sarika {
Preprocessing::Preprocessing(std::string srcdir, std::string destdir){
    src_dir = srcdir;
    dest_dir = destdir;
}

void Preprocessing::RawDataPreprocessing() {
    utils::File file(src_dir);
    utils::stringvec f;
    file.ReadDirectory(f);

    double counter_line = 0;
    for( int i = 0; i < f.size(); ++i ) {
        if(!file.Isfile(f[i])){
            std::string filename = f[i];
            std::string filename_org = filename;
            filename.insert(0, src_dir + "/");
            std::vector<std::vector<std::string>> data_each;
            std::cout << filename << std::endl;
            std::ifstream fileText(filename);
            std::string str;
            while ( std::getline(fileText, str) ) {
                std::vector<std::string> sample;
                std::string line = utils::Trim(str);
                if(line.empty() || line.rfind(SCRIPT_COMMENT, 0) == 0){
                    continue;
                }
                std::cout << line.compare(SCRIPT_SEPERATOR) << std::endl;
                if (line.compare(SCRIPT_SEPERATOR) == 0){
                    if(sample.size()){
                        data_each.push_back(sample);
                    }
                    sample.clear();
                }else{
                     sample.push_back(line);
                }
                if(sample.size())
                    data_each.push_back(sample);

            }


            // std::cout << "data_each"  << data_each << std::endl;
            counter_line += data_each.size();

            FILE *pOut = fopen(filename_org.insert(0, dest_dir + "/").c_str(), "w");
            for( int i = 0; i < data_each.size() -1 ; i += 2 ) {
                for( int j = 0; j < data_each[i].size(); ++j ){

                    std::vector<std::string> list_word_question = utils::WordTokenize(data_each[i][j]);
                    std::vector<std::string> list_word_answer = utils::WordTokenize(data_each[i+1][j]);
                    
                    vocabs.push_back(list_word_question);
                    vocabs.push_back(list_word_answer);
                    
                    std::string result_list_word_question = utils::Join(list_word_question.begin(), list_word_question.end(), std::string(" "));
                    std::string result_list_word_answer = utils::Join(list_word_answer.begin(), list_word_answer.end(), std::string(" "));
                    std::cout << list_word_answer << std::endl;
                    utils::ReplaceStringReg(result_list_word_question);
                    utils::ReplaceStringReg(result_list_word_answer);
                    std::vector<std::string> pair;
                    pair.push_back(result_list_word_question);
                    pair.push_back(result_list_word_answer);
                    qa.push_back(pair);
                    const char * indecate = i == data_each.size() - 2 ? ">> %s\n > %s":">> %s\n > %s\n";
                    fprintf(pOut, indecate, result_list_word_question.c_str(), result_list_word_answer.c_str() );
                }
            }
            fclose(pOut);
        }
    }
    std::cout << qa.size() << std::endl;
}

void Preprocessing::BuildVocabulary(std::vector<std::string> &vocabss){
    std::string tokens[] = {"_pad_","_unk_", "_bos_", "_eos_",
                            ".", "!", "?", "(", "[", "{", "``", "$",
                            "0", "1", "2", "3", "4", "5", "6",
                           "7", "8", "9", "។"};
    
    for ( int i = 0; i < sizeof(tokens)/sizeof(tokens[0]); ++i ){
        vocabss.push_back(tokens[i]);
    }
//     std::vector<std::string> word_data;
//     utils::File::ReadFile2Vect(word_data, "../data/words.txt");
//     std::cout << word_data << std::endl;
//     vocabss.insert(std::end(vocabss), std::begin(word_data), std::end(word_data));
    for ( int i = 0; i < vocabs.size(); ++i ){
        for ( int j = 0; j < vocabs[i].size(); ++j ){
            if (std::find(vocabss.begin(), vocabss.end(), vocabs[i][j]) != vocabss.end()){
                continue;
            }
            vocabss.push_back(vocabs[i][j]);
        }
    }
    std::string vocab = "vocab.txt";
    FILE *pOut = fopen(vocab.insert(0, dest_dir + "/").c_str(), "w");
    for ( int i = 0; i < vocabss.size(); ++i ){
        const char * voca = i == vocabss.size() - 1?"%s":"%s\n";
        fprintf(pOut, voca ,vocabss[i].c_str());
    }
    fclose(pOut);
}

void Preprocessing::PrepareDataset(float test_size){
    int sep_test = qa.size() * 2 * test_size;
    std::vector<std::vector<std::string>> test_data(qa.begin(), qa.begin() + sep_test);
    std::vector<std::vector<std::string>> traing_data(qa.begin() + sep_test, qa.end());
    std::cout << test_data.size() + traing_data.size() << std::endl;
    FILE * fTest_enc = fopen((dest_dir + "/dataset/"+ TESTING_ENC_FILE).c_str(), "w");
    FILE * fTest_dec = fopen((dest_dir + "/dataset/"+ TESTING_DEC_FILE).c_str(), "w");
    for( int i = 0; i < test_data.size(); ++i ) {
       for( int j = 0; j < test_data[i].size(); ++j ){
            const char * word = i == test_data.size() - 1 ?"%s":"%s\n";
            if (j % 2 == 0)
                fprintf(fTest_enc, word, test_data[i][j].c_str());
            else {
                fprintf(fTest_dec, word, test_data[i][j].c_str());
            }

       }
    }
    FILE * fTrain_enc = fopen((dest_dir + "/dataset/"+ TRAINING_ENC_FILE).c_str(), "w");
    FILE * fTrain_dec = fopen((dest_dir + "/dataset/"+ TRAINING_DEC_FILE).c_str(), "w");
    for( int i = 0; i < traing_data.size(); ++i ) {
       for( int j = 0; j < traing_data[i].size(); ++j ){
            const char * word = i == traing_data.size() - 1 ?"%s":"%s\n";
            if (j % 2 == 0)
                fprintf(fTrain_enc, word, traing_data[i][j].c_str());
            else{
                fprintf(fTrain_dec, word, traing_data[i][j].c_str());
            }

       }
    }
    fclose(fTest_dec);
    fclose(fTest_enc);
    fclose(fTrain_dec);
    fclose(fTrain_dec);
}

}