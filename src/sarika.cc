#include <iostream>
#include "core.h"
#include "utils/file.h"
#include <tensorflow/core/platform/init_main.h>
#include <tensorflow/core/util/command_line_flags.h>
#include <tensorflow/core/platform/logging.h>
#include <string>
#include <vector>
#include "preprocessing.h"
#include "utils/config.h"
#include "utils/stringutils.h"
#include "tokenizer.h"
#include <map>
using namespace tensorflow;



int main(int argc, char** argv)
{

    //do text detection
    std::string mode = "";
    std::string config = "";
    std::vector<Flag> flag_list = {
        Flag("mode", &mode, "the mode, must be within the three categories: preprocess_raw, train, chat"),
        Flag("config", &config, "Config dir"),
    };

    std::string usage = Flags::Usage(argv[0], flag_list);
    const bool parse_result = Flags::Parse(&argc, argv, flag_list);

    if (!parse_result) {
        LOG(ERROR) << usage;
        return -1;
    }

    ::tensorflow::port::InitMain(argv[0], &argc, &argv);
    if (argc > 1) {
        LOG(ERROR) << "Unknown argument " << argv[1] << "\n" << usage;
        return -1;
    }

    std::map<std::string, std::string> m_cfgMap;
    utils::Init(m_cfgMap, config);

    std::cout << m_cfgMap["raw_data_dir"] << std::endl;
    sarika::Preprocessing preprocessing(m_cfgMap["raw_data_dir"], m_cfgMap["dest_dir"]);
    sarika::Tokenizer tokenizer(m_cfgMap);
    if(mode == "preprocess_raw"){
        preprocessing.RawDataPreprocessing();
        std::vector<std::string> vocab;
        preprocessing.BuildVocabulary(vocab);
        preprocessing.PrepareDataset(0.1);
    }
    else if(mode == "tokenize"){
        tokenizer.ToId("train", sarika::ENC);
        sleep(0.2);
        tokenizer.ToId("train", sarika::DEC);
        tokenizer.ToId("test", sarika::ENC);
        tokenizer.ToId("test", sarika::DEC);

    }else if(mode == "train"){


    }else if(mode == "chat"){

    }else if(mode == "test"){
        std::cout<< "word segmentation" << std::endl;
        utils::Segementation("hello");
    }else{
        LOG(ERROR) << "mode should be within: detect, recognize, detect_and_read";
    }

    return 0;
}
