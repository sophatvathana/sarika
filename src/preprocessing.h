#ifndef SARIKA_PREPROCESSING_H
#define SARIKA_PREPROCESSING_H
#include <string>
#include <vector>
#include <iostream>
#define SCRIPT_SEPERATOR ">>>"
#define SCRIPT_COMMENT "#>>>"
#define TRAINING_ENC_FILE "train.enc" 
#define TRAINING_DEC_FILE "train.dec" 
#define TESTING_ENC_FILE "test.enc"
#define TESTING_DEC_FILE "test.dec"

enum Lang { KH, EN };
namespace sarika {


class Preprocessing {
    public:
        Preprocessing(std::string src_dir, std::string dest_dir);
        void RawDataPreprocessing();
        void BuildVocabulary(std::vector<std::string> &vocabs);
        void PrepareDataset(float test_size = 0.2);
    private:
        std::string dest_dir;
        std::string src_dir;
        std::vector<std::vector<std::string>> vocabs;
        std::vector<std::vector<std::string>> qa;

};
}

#endif