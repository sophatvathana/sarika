#ifndef SARIKA_MODEL_H
#define SARIKA_MODEL_H
#include <string>
namespace sarika {
struct Attenction { 
    const std::string BAHDANAU_MECHANISM = "bahdanau";
    const std::string NORMED_BAHDANAU_MECHANISM = "normed_bahdanau";

    const std::string LUONG_MECHANISM = "luong";
    const std::string SCALED_LUONG_MECHANISM = "scaled_luong";
    Attenction(std::string attention_mechanism="bahdanau",
                std::string encoder_type="bi",
                 int num_units=512,
                 bool memory=false,
                 bool memory_sequence_length=false);
    
};

}

#endif