#include "tokenizer.h"
#include "core.h"
#include "utils/stringutils.h"
#include "utils/file.h"
#include <map>
#include <string>
#include <iostream>
#include <fstream>
// #include "tensorflow/core/framework/op_kernel.h"
#include <vector>

namespace sarika {
const std::string proc_types[] = {"dec", "enc"};

Tokenizer::Tokenizer(std::map<std::string, std::string> config){
    this->config = config;
    std::vector<std::string> data;
    utils::File::ReadFile2Vect(data, config["dest_dir"] + "/vocab.txt");
    this->vocab_size = data.size();
    this->vocabs = data;
    std::cout << this->vocabs << std::endl;
}

std::vector<std::string> Tokenizer::GetVocabs() {
    return this->vocabs;
}

std::vector<std::string> Tokenizer::Sentence2Id(std::string line) {
    std::vector<std::string> sentenceids;
    std::vector<std::string> segLine = utils::Explode(line, ' ');
    std::cout << "segLine" << segLine << std::endl;
    for(auto w:segLine){
        if(int id = this->vocabIds[w]){
            sentenceids.push_back(std::to_string(id));
        }else {
            sentenceids.push_back(std::to_string(this->vocabIds["_unk_"]));
        }
    }
    return sentenceids;
}

void Tokenizer::Vocab2Id(){
    for (int i = 0; i < this->vocabs.size(); ++i){
        vocabIds.insert(make_pair(this->vocabs[i], i));
    }
}

void Tokenizer::ToId(std::string name, PROC_TYPE type) {
    std::string proc_t = proc_types[type];
    std::string in_path = name + "." + proc_t;
    std::string out_path = name + "_ids." + proc_t;
    in_path = config["dest_dir"] + "/dataset/" + in_path;
    out_path = config["dest_dir"] + "/dataset/" + out_path;
    FILE *fOutFile = fopen(out_path.c_str(), "w");
    std::string line;
    this->Vocab2Id();
    std::ifstream fInFile(in_path.c_str());
    while(std::getline(fInFile, line)){
        std::cout<< "Indecate here" << line << std::endl;
        std::vector<std::string> ids;
        if(proc_t == "dec") // we only care about '_bos_' and _eos_ in decoder
            ids.push_back(std::to_string(this->vocabIds["_bos_"]));
        std::vector<std::string> sentence_ids = Sentence2Id(line);
        ids += sentence_ids;
        if (proc_t == "dec")
            ids.push_back(std::to_string(this->vocabIds["_eos_"]));
        std::string str_join = utils::Join(ids.begin(), ids.end(), std::string(" "));
        std::cout << "EOF " << std::to_string(fInFile.eof()) << std::endl;
        const char * word = fInFile.eof() == 1 ? "%s":"%s\n";
        fprintf(fOutFile, word, str_join.c_str());
       
    }
    fclose(fOutFile);
}

}