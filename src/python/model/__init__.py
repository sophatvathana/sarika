
from ..config import Config
import tensorflow as tf

from .model import Encoder
from .model import Decoder
import io
import numpy as np
from src.python import dataset
import os 

class Graph:

    def __init__(self, mode=None, dtype=tf.float32):
        self.mode = mode
        self.beam_width = int(Config.get('predict')['beam_width'])
        self.dtype = dtype
        print("Model got vocab size: ", Config.get('data')['vocab_size'])
        self.global_step = tf.Variable(0, trainable=False)
        self.time_major = eval(Config.get('model')["time_major"])
    #Will unessessery 
    def forward_model(self, sess, state, input_sample):
        '''Run a forward pass. Return the updated hidden state and the output probabilities.'''
        shaped_input = np.array([[input_sample]], np.float32)
        inputs = {self.encoder_inputs: shaped_input}
        self.add_state_to_feed_dict(inputs, state)
        [probs, state] = sess.run([self.probs, self.encoder_final_state], feed_dict=inputs)
        return probs[0], state

    def build(self,
              encoder_inputs=None,
              decoder_inputs=None):

        if self.time_major:
            encoder_inputs = tf.transpose(encoder_inputs)
            decoder_inputs = tf.transpose(decoder_inputs)
            
        # set inputs variable
        self.encoder_inputs = encoder_inputs
        self.encoder_input_lengths = tf.reduce_sum(
            tf.to_int32(tf.not_equal(self.encoder_inputs, int(Config.get('data')['PAD_ID']) )), 
            1,
            name="encoder_input_lengths")

        if self.mode == tf.estimator.ModeKeys.TRAIN or self.mode == tf.estimator.ModeKeys.EVAL:
            self.decoder_inputs = decoder_inputs
            self.decoder_input_lengths = tf.reduce_sum(
                tf.to_int32(tf.not_equal(self.decoder_inputs, int(Config.get('data')['PAD_ID']) )), 1,
                name="decoder_input_lengths")
        else:
            self.decoder_inputs = None
            self.decoder_input_lengths = None

        self._build_embed()
        self._build_encoder()
        self._build_decoder()
   
    def word_embedding_matrix(self, embedding_path,vocab,dim):
    
        #first and second vector are pad and unk words

        with open(embedding_path,'r') as f:
            word_vocab =[]
            embedding_matrix = []
            word_vocab.extend(['_pad_','_unk_'])
            embedding_matrix.append(np.random.uniform(-1.0, 1.0, (1,dim))[0])
            embedding_matrix.append(np.random.uniform(-1.0, 1.0, (1,dim))[0])


            for line in f:
                if line.split()[0] in vocab:
                    word_vocab.append(line.split()[0])
                    embedding_matrix.append([float(i) for i in line.split()[1:]])


        return {'word_vocab': word_vocab,'Embedding_matrix': np.reshape(embedding_matrix,[-1,dim]).astype(np.float32)}


    def _build_embed(self):
        with tf.variable_scope ("embeddings", dtype=self.dtype) as scope:
            pretrained_embedding = self.word_embedding_matrix("data/cc.km.300.vec",dataset.load_vocab(os.path.join(Config.get("data")["base_path"], "vocab.txt")), int(Config.get('model')['embed_dim']))
            load_embedding_matrix = pretrained_embedding['Embedding_matrix']
            shape_word_vocab      = pretrained_embedding['word_vocab']
            print(load_embedding_matrix)
            if Config.get('model')['embed_share']:
                embedding = tf.get_variable(
                    "embedding_share", [ int(Config.get('data')['vocab_size']) , int(Config.get('model')['embed_dim']) ], self.dtype, initializer=tf.constant_initializer(load_embedding_matrix))

                self.embedding_encoder = embedding
                self.embedding_decoder = embedding
            else:
                self.embedding_encoder = tf.get_variable(
                    "embedding_encoder", [int(Config.get('data')['vocab_size']), int(Config.get('model')['embed_dim']) ], self.dtype, initializer=tf.constant_initializer(load_embedding_matrix) )
                self.embedding_decoder = tf.get_variable(
                    "embedding_decoder", [int(Config.get('data')['vocab_size']), int(Config.get('model')['embed_dim']) ], self.dtype, initializer=tf.constant_initializer(load_embedding_matrix) )
           
            # if self.time_major:
            #     self.decoder_inputs = tf.transpose(self.decoder_inputs)

            self.encoder_emb_inp = tf.nn.embedding_lookup(
                self.embedding_encoder, self.encoder_inputs)

            if self.mode == tf.estimator.ModeKeys.TRAIN:
                self.decoder_emb_inp = tf.nn.embedding_lookup(
                    self.embedding_decoder, self.decoder_inputs)
            else:
                self.decoder_emb_inp=None

    def _build_encoder(self):
        with tf.variable_scope('encoder'):
            encoder = Encoder(
                    encoder_type=Config.get('model')['encoder_type'],
                    num_layers=int(Config.get('model')['num_layers']),
                    cell_type=Config.get('model')['cell_type'],
                    num_units=int(Config.get('model')['encoder_num_units']),
                    dropout=float(Config.get('model')['dropout']))

            self.encoder_outputs, self.encoder_final_state = encoder.build(
                    input_vector=self.encoder_emb_inp,
                    sequence_length=self.encoder_input_lengths)

            if self.mode == tf.estimator.ModeKeys.PREDICT and self.beam_width > 0:
                self.encoder_outputs = tf.contrib.seq2seq.tile_batch(
                        self.encoder_outputs, self.beam_width)
                self.encoder_input_lengths = tf.contrib.seq2seq.tile_batch(
                        self.encoder_input_lengths, self.beam_width)

    def _build_decoder(self):

        batch_size = tf.shape(self.encoder_inputs)[0]

        with tf.variable_scope('decoder'):

            decoder = Decoder(
                        cell_type=Config.get('model')['cell_type'],
                        dropout=float(Config.get('model')['dropout']),
                        encoder_type=Config.get('model')['encoder_type'],
                        num_layers=int(Config.get('model')['num_layers']),
                        num_units=int(Config.get('model')['decoder_num_units']),
                        sampling_probability=float(Config.get('train')['sampling_probability']),
                        mode=self.mode,
                        dtype=self.dtype)

            decoder.set_attention_then_project(
                        attention_mechanism=Config.get('model')['attention_mechanism'],
                        beam_width=self.beam_width,
                        memory=self.encoder_outputs,
                        memory_sequence_length=self.encoder_input_lengths,
                        vocab_size=int(Config.get('data')['vocab_size']))
            decoder.set_initial_state(batch_size, self.encoder_final_state)

            decoder_outputs = decoder.build(
                                inputs=self.decoder_emb_inp,
                                sequence_length=self.decoder_input_lengths,
                                embedding=self.embedding_decoder,
                                start_tokens=tf.fill([batch_size], int(Config.get('data')['START_ID'])),
                                end_token=int(Config.get('data')['EOS_ID']),
                                length_penalty_weight=float(Config.get('predict')['length_penalty_weight']))

            if self.mode == tf.estimator.ModeKeys.TRAIN:
                self.decoder_logits = decoder_outputs.rnn_output
            else:
                if self.mode == tf.estimator.ModeKeys.PREDICT and self.beam_width > 0:
                    self.decoder_logits = tf.no_op()
                    predicted_ids = decoder_outputs.predicted_ids
                    self.predictions = predicted_ids
                else:
                    self.decoder_logits = decoder_outputs.rnn_output
                    self.predictions = decoder_outputs.sample_id

            if self.mode == tf.estimator.ModeKeys.PREDICT:
                # PREDICT mode do not need loss
                return

            decoder_output_length = tf.shape(self.decoder_logits)[1]

            def concat_zero_padding():
                pad_num = int(Config.get('data')['max_seq_length']) - decoder_output_length
                zero_padding = tf.zeros(
                        [batch_size, pad_num, int(Config.get('data')['vocab_size']) ],
                        dtype=self.dtype)

                return tf.concat([self.decoder_logits, zero_padding], axis=1)

            def slice_to_max_len():
                return tf.slice(self.decoder_logits,
                                [0, 0, 0],
                                [batch_size, int(Config.get('data')['max_seq_length']), int(Config.get('data')['vocab_size'])
                                ])

            # decoder output sometimes exceed max_seq_length
            self.logits = tf.cond(decoder_output_length < int(Config.get('data')['max_seq_length']),
                                  concat_zero_padding,
                                  slice_to_max_len)
            self.predictions = tf.argmax(self.logits, axis=2)

            self.weight_masks = tf.sequence_mask(
                lengths=self.decoder_input_lengths,
                maxlen=int(Config.get('data')['max_seq_length']),
                dtype=self.dtype, name='masks')

        if self.mode == tf.estimator.ModeKeys.TRAIN:
            self.train_predictions = tf.argmax(self.logits, axis=2)
            # for print trainig data
            tf.identity(tf.argmax(self.decoder_logits[0], axis=1), name='train/pred_0')
            
        
