#-*- coding: utf-8 -*-
import os
import sys
sys.path.insert(0, os.path.abspath(os.path.realpath('.')))
import argparse
from src.python.config import Config
import numpy as np
import tensorflow as tf
from src.python import dataset
from src.python.sarika import Sarika
from src.python import utils
import json
from flask import Flask, session
import flask
import re
from flask_cors import CORS
from flask_socketio import SocketIO
from flask_socketio import send, emit
from flask_session.__init__ import Session
import datetime

func_regex = r"@(\w+)@"
param_ex = r"$(\w+)$"
return_name_regex = r"(\s|^)ខ្ញុំ\s*ឈ្មោះ\s*([^,|.|។|\s]*|.+?\.|\។|\,\s|\S*|$)"   

Config("config/config_v5.5.cfg")
# Config.get('train')['batch_size'] = str(1)

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
tf.logging.set_verbosity(tf.logging.ERROR)

app = Flask(__name__)
CORS(app)
app.config['SESSION_TYPE'] = 'filesystem'
Session(app)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
socketio = SocketIO(app, async_mode=None, manage_session=False)

def _make_estimator():
    config_model = {key:Config.get('model')[key] for key in Config.get('model')}
    params = tf.contrib.training.HParams(**config_model)
    # Using CPU
    run_config = tf.contrib.learn.RunConfig(
        model_dir=Config.get('train')['model_dir'],
        session_config=tf.ConfigProto(
            device_count={'GPU': -1}
        ))

    sarika = Sarika()
    return tf.estimator.Estimator(
            model_fn=sarika.model_fn,
            model_dir=Config.get('train')['model_dir'],
            params=params,
            config=run_config)

estimator = _make_estimator()

def initial_state(net, sess):
    # Return freshly initialized model states.
    return sess.run(net.zero_state)

def consensus_length(beam_outputs, early_term_token):
    for l in range(len(beam_outputs[0])):
        if l > 0 and beam_outputs[0][l-1] == early_term_token:
            return l-1, True
        for b in beam_outputs[1:]:
            if beam_outputs[0][l] != b[l]: return l, False
    return l, False

def beam_search_generator(sess, net, initial_state, initial_sample,
    early_term_token, beam_width, forward_model_fn, forward_args):
    '''Run beam search! Yield consensus tokens sequentially, as a generator;
    return when reaching early_term_token (newline).
    Args:
        sess: tensorflow session reference
        net: tensorflow net graph (must be compatible with the forward_net function)
        initial_state: initial hidden state of the net
        initial_sample: single token (excluding any seed/priming material)
            to start the generation
        early_term_token: stop when the beam reaches consensus on this token
            (but do not return this token).
        beam_width: how many beams to track
        forward_model_fn: function to forward the model, must be of the form:
            probability_output, beam_state =
                    forward_model_fn(sess, net, beam_state, beam_sample, forward_args)
            (Note: probability_output has to be a valid probability distribution!)
        tot_steps: how many tokens to generate before stopping,
            unless already stopped via early_term_token.
    Returns: a generator to yield a sequence of beam-sampled tokens.'''
    # Store state, outputs and probabilities for up to args.beam_width beams.
    # Initialize with just the one starting entry; it will branch to fill the beam
    # in the first step.
    beam_states = [initial_state] # Stores the best activation states
    beam_outputs = [[initial_sample]] # Stores the best generated output sequences so far.
    beam_probs = [1.] # Stores the cumulative normalized probabilities of the beams so far.

    while True:
        # Keep a running list of the best beam branches for next step.
        # Don't actually copy any big data structures yet, just keep references
        # to existing beam state entries, and then clone them as necessary
        # at the end of the generation step.
        new_beam_indices = []
        new_beam_probs = []
        new_beam_samples = []

        # Iterate through the beam entries.
        for beam_index, beam_state in enumerate(beam_states):
            beam_prob = beam_probs[beam_index]
            beam_sample = beam_outputs[beam_index][-1]

            # Forward the model.
            prediction, beam_states[beam_index] = forward_model_fn(
                    sess, net, beam_state, beam_sample, forward_args)

            # Sample best_tokens from the probability distribution.
            # Sample from the scaled probability distribution beam_width choices
            # (but not more than the number of positive probabilities in scaled_prediction).
            count = min(beam_width, sum(1 if p > 0. else 0 for p in prediction))
            best_tokens = np.random.choice(len(prediction), size=count,
                                            replace=False, p=prediction)
            for token in best_tokens:
                prob = prediction[token] * beam_prob
                if len(new_beam_indices) < beam_width:
                    # If we don't have enough new_beam_indices, we automatically qualify.
                    new_beam_indices.append(beam_index)
                    new_beam_probs.append(prob)
                    new_beam_samples.append(token)
                else:
                    # Sample a low-probability beam to possibly replace.
                    np_new_beam_probs = np.array(new_beam_probs)
                    inverse_probs = -np_new_beam_probs + max(np_new_beam_probs) + min(np_new_beam_probs)
                    inverse_probs = inverse_probs / sum(inverse_probs)
                    sampled_beam_index = np.random.choice(beam_width, p=inverse_probs)
                    if new_beam_probs[sampled_beam_index] <= prob:
                        # Replace it.
                        new_beam_indices[sampled_beam_index] = beam_index
                        new_beam_probs[sampled_beam_index] = prob
                        new_beam_samples[sampled_beam_index] = token
        # Replace the old states with the new states, first by referencing and then by copying.
        already_referenced = [False] * beam_width
        new_beam_states = []
        new_beam_outputs = []
        for i, new_index in enumerate(new_beam_indices):
            if already_referenced[new_index]:
                new_beam = copy.deepcopy(beam_states[new_index])
            else:
                new_beam = beam_states[new_index]
                already_referenced[new_index] = True
            new_beam_states.append(new_beam)
            new_beam_outputs.append(beam_outputs[new_index] + [new_beam_samples[i]])
        # Normalize the beam probabilities so they don't drop to zero
        beam_probs = new_beam_probs / sum(new_beam_probs)
        beam_states = new_beam_states
        beam_outputs = new_beam_outputs
        # Prune the agreed portions of the outputs
        # and yield the tokens on which the beam has reached consensus.
        l, early_term = consensus_length(beam_outputs, early_term_token)
        if l > 0:
            for token in beam_outputs[0][:l]: yield token
            beam_outputs = [output[l:] for output in beam_outputs]
        if early_term: return

def get_out_put_from_tokens_beam_search(all_sentences, rev_vocab):
    all_string_sent = []
    string_sent = []
    for each_word in all_sentences[:,0]:
        string_sent.append(rev_vocab.get(each_word))
    all_string_sent.append(' '.join(string_sent))
    return all_string_sent

vocab = dataset.load_vocab(os.path.join(Config.get("data")["base_path"],"vocab.txt"))
Config.get('data')['vocab_size'] = str(len(vocab))
rev_vocab = utils.get_rev_vocab(vocab)

def chat(ids, vocab):
#     states = initial_state()
    
    paded_ids = dataset._pad_input(ids, int(Config.get('data')['max_seq_length']))
    X = np.array(paded_ids, dtype=np.int32)
    print("ids",X)
    X = np.reshape(X, (1, int(Config.get('data')['max_seq_length'])))
#     X = np.reshape(X, (1, len(X)))
    predict_input_fn = tf.estimator.inputs.numpy_input_fn(
            x={"input_data": X},
            num_epochs=1,
            shuffle=False)

    result = estimator.predict(input_fn=predict_input_fn)
    print(result)
    prediction = next(result)["prediction"]
#     print(prediction)
    beam_width = int(Config.get('predict')['beam_width'])
    print(beam_width)
    if beam_width > 0:
#         print([[rev_vocab.get(x, '') for x in pre] for pre in list(prediction)])
        prediction = get_out_put_from_tokens_beam_search(prediction, rev_vocab)
        print("prediction", prediction)
        return ' '.join(prediction)
    def to_str(sequence):
        print("sequence", sequence)
        tokens = [
            rev_vocab.get(x, '') for x in sequence if x != int(Config.get('data')['PAD_ID'])
            ]
        print("tokens", tokens)
        return ' '.join(tokens)

    return to_str(prediction)

def main(sentence):
    
    ids = dataset.sentence2id(vocab, sentence)
    ids += [int(Config.get('data')['START_ID'])]
    answer = chat(ids, vocab)
    answer = parse_answer(answer)
    return answer

def ask_name(answer):
    print("username",session.get('username', "not set"))
    string = "តើអ្នកមានឈ្មោះអ្វី?"
    if not session.get('username', False) or session.get('ask_name', True):
        answer = re.sub(func_regex, string, answer)
    
    if session.get('username', False) or not session.get('ask_name', True):
        answer = re.sub(func_regex, session.get('username'), answer)

    return answer

def return_name(answer):
    print("username",session.get('username', "not set"))
    return "រីករាយណាស់ដែលបានស្គាល់ {} {}".format(session.get("username",""), "!")

def parse_function(answer):
    matches = re.findall(func_regex, answer, re.MULTILINE | re.UNICODE)
    if len(matches) == 0:
        matches = re.findall(param_ex, answer, re.MULTILINE | re.UNICODE)
    
    if len(matches) == 0:
        return answer, False

    func = matches[0]
    print("func", func)
    return answer, func

def get_time(answer):
    answer = re.sub(func_regex, datetime.datetime.now().strftime("%H:%M"), answer)
    return answer

def get_date_today(answer):
    answer = re.sub(func_regex, datetime.datetime.now().strftime("ទី %d ខែ %m ឆ្នាំ %Y"), answer)
    return answer

def get_date_tomorrow(answer):
    answer = re.sub(func_regex, (datetime.date.today() + datetime.timedelta(days=1)).strftime("ទី %d ខែ %m ឆ្នាំ %Y"), answer)
    return answer

def get_click_here(answer):
    answer = re.sub(func_regex, '<a target="_blank" href="https://www.smart.com.kh/">click here</a>', answer)
    return answer

def execute_func(func_name):
    switch = {
        "ask_name": ask_name,
        "return_name": return_name,
        "get_time": get_time,
        "get_date_today": get_date_today,
        "get_date_tomorrow": get_date_tomorrow,
        "click_here": get_click_here,
    }
    return switch.get(func_name, False)

def parse_answer(answer):
    q, function = parse_function(answer)
    if function == False:
        return answer 
    funct = execute_func(function)
    if not funct:
        return answer

    return funct.__call__(q)

def parse_question(q):
    print("___start___ parsing question")
    test_str = q
    param = False
    matches = re.findall(return_name_regex, test_str, re.MULTILINE | re.UNICODE)
    if len(matches) != 0 and session.get('ask_name', True):
        param = matches[0][1]
        session['username'] = param
        session['ask_name'] = False

    if len(matches) == 0:
        return q, False

    return q, param
    
@app.route('/chat', methods=['POST'])
def classify():
    question = flask.request.form.get('q')
    print("question" ,question)
    q, param = parse_question(question)
    print(q)
    answer = main(q)
    print(session.get('username', False))
    print(answer)
    resp = { "code":100,
            "answer" : answer
            }

    return flask.Response(json.dumps(resp), mimetype="application/json")

@socketio.on('message')
def handle_message(message):
    print('received message: ' + message)
    q, param = parse_question(message)
    print(q)
    answer = main(q)
    print(session.get('username', False))
    print(answer)
    send(answer)


if __name__ == "__main__":
    socketio.run(app, host='0.0.0.0', debug=True, port=3010)           
                  