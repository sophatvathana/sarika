import configparser
import os
import json
class ConfigMeta(type):
    class __ConfigMeta:
        def __init__(self):
            self.config = configparser.RawConfigParser()
            # self.config.read(file_dir)

        def __call__(self, file_dir):
            self.file_dir = file_dir
            self.config.read(file_dir)

        def get(self, name, default=None):
            try:
                return self.__getattr__(name)
            except KeyError as ke:
                return default 

        def __getattr__(self, name):
            self._set_config()

            config_value = self.config[name]
            if type(config_value) == dict:
                return SubConfig(config_value, get_tag=name)
            else:
                return config_value

        def _set_config(self):
            if self.config is None:
                self.config.read(self.file_dir)

        
        def __repr__(self):
            if self.config is None:
                raise FileNotFoundError(f"No such files start filename '{self.file_dir}'")
            else:
                return self.config

    instance = None
    def __new__(cls, *args, **kwargs):
        if not cls.instance:
            cls.instance = cls.__ConfigMeta()
        return cls.instance

class Config(metaclass=ConfigMeta):
    pass

class SubConfig:

    def __init__(self, *args, get_tag=None):
        self.get_tag = get_tag
        self.__dict__ = dict(*args)

    def __getattr__(self, name):
        if name in self.__dict__["__dict__"]:
            item = self.__dict__["__dict__"][name]
            if type(item) == dict:
                return SubConfig(item, get_tag=self.get_tag+"."+name)
            else:
                return item
        else:
            raise AttributeError("No such attribute: " + name)

    def __setattr__(self, name, value):
        self.__dict__[name] = value
        if name != "get" and name != "__dict__":
            origin_config = Config.config
            gets = self.get_tag.split(".")
            for get in gets:
                origin_config = origin_config[get]

            origin_config[name] = value

    def get(self, name, default=None):
        return self.__dict__["__dict__"].get(name, default)

    def to_dict(self):
        return self.__dict__["__dict__"]

    def __repr__(self):
        return json.dumps(self.__dict__["__dict__"], indent=4)