#-- coding: utf-8 -*-
import os
import sys
sys.path.insert(0, os.path.abspath(os.path.realpath('.')))
import math
import time
import argparse
import atexit
import logging
from src.python.model import Graph
import configparser
import tensorflow as tf
from src.python.config import Config
from src.python.dataset import Dataset
from src.python.conversation import Conversation
from src.python.model.model2 import ModelCreator

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

class Trainer():
    def __init__(self, config):
        self.config = config
        self.graph = tf.Graph()
        with self.graph.as_default():
            dataset = Dataset(config = self.config) 
            self.train_batch = dataset.get_training_batch()
            self.model = ModelCreator(training=True,                tokenized_data=dataset,
            batch_input=self.train_batch)
            
    
    def train(self, target=""):
        """Train a seq2seq model."""
        # Summary writer
        summary_name = "train_log"
        summary_writer = tf.summary.FileWriter(os.path.join(self.config.get("train")["model_dir"], summary_name), self.graph)

        log_device_placement = self.config.get("train")["log_device_placement"] == "True"
        num_epochs = int(self.config.get("train")["num_epochs"])

        config_proto = tf.ConfigProto(log_device_placement=log_device_placement,
                                      allow_soft_placement=True)
        config_proto.gpu_options.allow_growth = True

        with tf.Session(target=target, config=config_proto, graph=self.graph) as sess:
            sess.run(tf.global_variables_initializer())
            sess.run(tf.tables_initializer())
            global_step = self.model.global_step.eval(session=sess)
            
            # Initialize all of the iterators
            sess.run(self.train_batch.initializer)

            # Initialize the statistic variables
            ckpt_loss, ckpt_predict_count = 0.0, 0.0
            train_perp, last_record_perp = 2000.0, 2.0
            train_epoch = 0

            print("# Training loop started @ {}".format(time.strftime("%Y-%m-%d %H:%M:%S")))
            epoch_start_time = time.time()
            conversation = Conversation()
            while train_epoch < num_epochs:
                # Each run of this while loop is a training step, multiple time/steps will trigger
                # the train_epoch to be increased.
                learning_rate = self.learning_rate(train_perp)
                try:
                    step_result = self.model.train_step(sess, learning_rate=learning_rate)
                    (_, step_loss, step_predict_count, step_summary, global_step,
                     step_word_count, batch_size) = step_result

                    # Write step summary.
                    summary_writer.add_summary(step_summary, global_step)

                    # update statistics
                    ckpt_loss += (step_loss * batch_size)
                    ckpt_predict_count += step_predict_count
                except tf.errors.OutOfRangeError:
                    # Finished going through the training dataset. Go to next epoch.
                    train_epoch += 1

                    mean_loss = ckpt_loss / ckpt_predict_count
                    train_perp = math.exp(float(mean_loss)) if mean_loss < 300 else math.inf

                    epoch_dur = time.time() - epoch_start_time
                    print("# Finished epoch {:2d} @ step {:5d} @ {}. In the epoch, learning rate = {:.6f}, "
                          "mean loss = {:.4f}, perplexity = {:8.4f}, and {:.2f} seconds elapsed."
                          .format(train_epoch, global_step, time.strftime("%Y-%m-%d %H:%M:%S"),
                                  learning_rate, mean_loss, train_perp, round(epoch_dur, 2)))
                    epoch_start_time = time.time()  # The start time of the next epoch

                    summary = tf.Summary(value=[tf.Summary.Value(tag="train_perp", simple_value=train_perp)])
                    summary_writer.add_summary(summary, global_step)

                    # Save checkpoint
                    if train_perp < 1.6 and train_perp < last_record_perp:
                        self.model.saver.save(sess, os.path.join(self.config.get("train")["model_dir"], "model"), global_step=global_step)
                        last_record_perp = train_perp

                    ckpt_loss, ckpt_predict_count = 0.0, 0.0

                    sess.run(self.model.batch_input.initializer)
                    continue

            # Done training
            self.model.saver.save(sess, os.path.join(self.config.get("train")["model_dir"], "model"), global_step=global_step)
            summary_writer.close()


    @staticmethod
    def learning_rate(train_perp):
        if train_perp <= 1.48:
            return 9.6e-5
        elif train_perp <= 1.64:
            return 1e-4
        elif train_perp <= 2.0:
            return 1.2e-4
        elif train_perp <= 2.4:
            return 1.6e-4
        elif train_perp <= 3.2:
            return 2e-4
        elif train_perp <= 4.8:
            return 2.4e-4
        elif train_perp <= 8.0:
            return 3.2e-4
        elif train_perp <= 16.0:
            return 4e-4
        elif train_perp <= 32.0:
            return 6e-4
        else:
            return 8e-4

    
if __name__ == "__main__":
    print("__start_training__")
    parser = argparse.ArgumentParser(
                        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--config', type=str, default='../../config',
                        help='config file name')
    parser.add_argument('--mode', type=str, default='train',
                        help='Mode (train/test/train_and_evaluate)')
    args = parser.parse_args()

    tf.logging.set_verbosity(logging.INFO)

    # Print Config setting
    Config(args.config)
    trainer = Trainer(Config)
    trainer.train()
    
