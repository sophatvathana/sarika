#-- coding: utf-8 -*-
import os
import sys
sys.path.insert(0, os.path.abspath(os.path.realpath('.')))
import argparse
import atexit
import logging
import configparser
import tensorflow as tf
from tensorflow.python import debug as tf_debug
from src.python.dataset import *
from src.python.sarika import Sarika
from src.python import hook
from src.python import utils
from src.python.config import Config
config = tf.ConfigProto()
config.gpu_options.allow_growth = True

def main(mode, config):
    params = tf.contrib.training.HParams(**config)
    if eval(Config.get("data")["is_auto_max_seq_length"]):
        set_max_seq_length(["test_ids.dec", "test_ids.enc", "train_ids.dec", "train_ids.enc"])
        
    run_config = tf.estimator.RunConfig(
            model_dir=Config.get("train")["model_dir"],
            save_checkpoints_steps=int(Config.get("train")["save_checkpoints_steps"]))
     
    sarika = Sarika()
    
    estimator = tf.estimator.Estimator(
            model_fn=sarika.model_fn,
            model_dir=Config.get("train")["model_dir"],
            params=params,
            config=run_config)

    vocab = load_vocab(os.path.join(Config.get("data")["base_path"],"vocab.txt"))
    Config.get("data")["vocab_size"] = str(len(vocab))

    train_X, test_X, train_y, test_y = make_train_and_test_set()

    train_input_fn, train_input_hook = make_batch((train_X, train_y), batch_size=int(Config.get("model")["batch_size"]))
    
    test_input_fn, test_input_hook = make_batch((test_X, test_y), batch_size=int(Config.get("model")["batch_size"]), scope="test")

    train_hooks = [train_input_hook]
    if eval(Config.get("train")["print_verbose"]):
        train_hooks.append(hook.print_variables(
            variables=['train/enc_0', 'train/dec_0', 'train/pred_0'],
            rev_vocab=utils.get_rev_vocab(vocab),
            every_n_iter=int(Config.get("train")["check_hook_n_iter"])))
        
    if eval(Config.get("train")["debug"]):
        train_hooks.append(tf_debug.LocalCLIDebugHook())
   
    if eval(Config.get("train")["early_stop"]):
        os.makedirs(estimator.eval_dir())
        early_stopping = tf.contrib.estimator.stop_if_no_decrease_hook(estimator, metric_name='loss', max_steps_without_decrease=1000,min_steps=100)
        train_hooks.append(early_stopping)
        
    
    eval_hooks = [test_input_hook]
    
    if eval(Config.get("train")["debug"]):
        print(Config.get("train")["debug"])
        eval_hooks.append(tf_debug.LocalCLIDebugHook())
    
    train_spec = tf.estimator.TrainSpec(input_fn=train_input_fn, max_steps=int(Config.get("train")["train_steps"]), hooks=train_hooks)
    eval_spec = tf.estimator.EvalSpec(input_fn=test_input_fn, hooks=eval_hooks, start_delay_secs=0, steps=int(Config.get("train")["train_steps"]))
    
    experiment = tf.estimator.train_and_evaluate(estimator, train_spec, eval_spec)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
                        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--config', type=str, default='../../config',
                        help='config file name')
    parser.add_argument('--mode', type=str, default='train',
                        help='Mode (train/test/train_and_evaluate)')
    args = parser.parse_args()

    tf.logging.set_verbosity(logging.INFO)

    # Print Config setting
    Config(args.config)
    print("Config: ", [key for key in Config.get('model')])

    for key in Config.get('config'):
        print(f" - {key}: {Config.get('config')[key]}")

    config_config = {key:Config.get('config')[key] for key in Config.get('config')}
    config_model = {key:Config.get('model')[key] for key in Config.get('model')}
    config_train = {key:Config.get('train')[key] for key in Config.get('train')}
    config_data = {key:Config.get('data')[key] for key in Config.get('data')}
    config_dic = { 'config':config_config, 'model':config_model, 'train':config_train, 'data':config_data }
    print(config_dic["train"]["save_checkpoints_steps"])
    main(args.mode, config_dic)
