# -*- coding: utf-8 -*-
from __future__ import print_function
import os
import sys
sys.path.insert(0, os.path.abspath(os.path.realpath('.')))
import argparse
import random
import re
from src.python.config import Config
import numpy as np
import tensorflow as tf
from tqdm import tqdm
from khmerml.utils.khmersegment import KhmerSegment
# Process Khmer Segmentation
def unicode_split(text):
  segment = KhmerSegment()
  segment.PATH = "thirdparty/khmerseq"
  file_name = segment.add_to_file(text)

  # Run command to convert the sentence into separated words
  command = 'cd ' + segment.PATH + ' && ./km-5tag-seg-test.sh model/km-5tag-seg-model sample/' + file_name + ' sample-out/'
  KhmerSegment.run_command(command)

  input_file = segment.PATH + '/sample/' + file_name
  output_file_w = segment.PATH + '/sample-out/' + file_name + '.w'
  output_file_c = segment.PATH + '/sample-out/' + file_name + '.c'

  # Read temporary file content
  result = KhmerSegment.read_file(output_file_w).split(' ')
  clean_res = [] 
  try:
    result.remove('\n')
  except:
    pass
  try:
    for re in result:
         clean_res.append(re.rstrip())
  except:
    pass

  print(clean_res)
  # Clean up temporary files
  KhmerSegment.remove_file(input_file)
  KhmerSegment.remove_file(output_file_w)
  KhmerSegment.remove_file(output_file_c)

  return clean_res

def sentence2id(vocab, line):
    return [vocab.get(token, vocab['_unk_']) for token in unicode_split(line)]

def make_dir(path):
    """ Create a directory if there isn't one already. """
    try:
        os.mkdir(path)
    except OSError:
        pass

def load_vocab(vocab_fname):
    print("load vocab ...")
    config_data = Config.get('data')
    print(config_data['base_path'])
    with open(vocab_fname, 'rb') as f:
        words = f.read().decode('utf-8').splitlines()
        print("vocab size:", len(words))
    return {words[i]: i for i in range(len(words))}



def make_train_and_test_set(shuffle=True, bucket=True):
    print("make Training data and Test data Start....")

    train_X, train_y = load_data('train_ids.enc', 'train_ids.dec')
    test_X, test_y = load_data('test_ids.enc', 'test_ids.dec')

    assert len(train_X) == len(train_y)
    assert len(test_X) == len(test_y)

    print(f"train data count : {len(train_X)}")
    print(f"test data count : {len(test_X)}")

    if shuffle:
        print("shuffle dataset ...")
        train_p = np.random.permutation(len(train_y))
        test_p = np.random.permutation(len(test_y))

        train_X, train_y = train_X[train_p], train_y[train_p]
        test_X, test_y = test_X[test_p], test_y[test_p]

    if bucket:
        print("sorted by inputs length and outputs length ...")
        train_X, train_y = zip(*sorted(zip(train_X, train_y), key=lambda x: len(x[0]) + len([x[1]])))
        test_X, test_y = zip(*sorted(zip(test_X, test_y), key=lambda x: len(x[0]) + len([x[1]])))

    return train_X, test_X, train_y, test_y

def load_data_org(enc_fname, dec_fname):
    enc_input_data = open(os.path.join(Config.get('data')['base_path'], enc_fname), 'r')
    dec_input_data = open(os.path.join(Config.get('data')['base_path'], dec_fname), 'r')
    enc_data, dec_data = [], []
    for e_line, d_line in tqdm(zip(enc_input_data.readlines(), dec_input_data.readlines())):
        enc_data.append(e_line)
        dec_data.append(d_line)
    return enc_data, dec_data

def load_data(enc_fname, dec_fname):
    enc_input_data = open(os.path.join(Config.get('data')['base_path'], Config.get('data')['dataset_path'], enc_fname), 'r')
    dec_input_data = open(os.path.join(Config.get('data')['base_path'], Config.get('data')['dataset_path'], dec_fname), 'r')

    enc_data, dec_data = [], []
    for e_line, d_line in tqdm(zip(enc_input_data.readlines(), dec_input_data.readlines())):
        e_ids = [int(id_) for id_ in e_line.split()]
        print(e_ids)
        d_ids = [int(id_) for id_ in d_line.split()]
        print(d_ids)
        
        if len(e_ids) == 0 or len(d_ids) == 0:
            continue

        if len(e_ids) <= int(Config.get('data')['max_seq_length']) and len(d_ids) < int(Config.get('data')['max_seq_length']):

            if abs(len(d_ids) - len(e_ids)) / (len(e_ids) + len(d_ids)) < float(Config.get('data')['sentence_diff']):
                enc_data.append(_pad_input(e_ids, int(Config.get('data')['max_seq_length'])))
                dec_data.append(_pad_input(d_ids, int(Config.get('data')['max_seq_length'])))

    print(f"load data from {enc_fname}, {dec_fname}...")
    return np.array(enc_data, dtype=np.int32), np.array(dec_data, dtype=np.int32)


def _pad_input(input_, size):
    pad_id = int(Config.get('data')['PAD_ID'])
    return input_ + [pad_id] * (size - len(input_))


def set_max_seq_length(dataset_fnames):

    max_seq_length = Config.data.get('max_seq_length', 10)

    for fname in dataset_fnames:
        print(fname)
        input_data = open(os.path.join(Config.get("data")["base_path"], Config.get("data")["dataset_path"], fname), 'r')

        for line in input_data.readlines():
            ids = [int(id_) for id_ in line.split()]
            seq_length = len(ids)

            if seq_length > int(max_seq_length):
                max_seq_length = seq_length

    Config.data.max_seq_length = max_seq_length
    print(f"Setting max_seq_length to Config : {max_seq_length}")


def make_batch(data, buffer_size=10000, batch_size=64, scope="train"):

    class IteratorInitializerHook(tf.train.SessionRunHook):
        """Hook to initialise data iterator after Session is created."""

        def __init__(self):
            super(IteratorInitializerHook, self).__init__()
            self.iterator_initializer_func = None

        def after_create_session(self, session, coord):
            """Initialise the iterator after the session has been created."""
            self.iterator_initializer_func(session)


    def get_inputs():

        iterator_initializer_hook = IteratorInitializerHook()

        def train_inputs():
            with tf.name_scope(scope):

                x, y = data
                # Define placeholders
                max_seq_length = int(Config.get('data')['max_seq_length'])
                input_placeholder = tf.placeholder(
                    tf.int32, [None, max_seq_length])
                output_placeholder = tf.placeholder(
                    tf.int32, [None, max_seq_length])
                
                # Build dataset iterator
                dataset = tf.data.Dataset.from_tensor_slices(
                    (input_placeholder, output_placeholder))

                if scope == "train":
                    dataset = dataset.repeat(None)  # Infinite iterations
                else:
                    dataset = dataset.repeat(1)  # 1 Epoch
                # dataset = dataset.shuffle(buffer_size=buffer_size)
                dataset = dataset.batch(batch_size)
                
       
                iterator = dataset.make_initializable_iterator()
                next_x, next_y = iterator.get_next()

                # with tf.Session() as sess:
                #     sess.run(iterator.initializer, 
                #     feed_dict={input_placeholder: x,
                #                    output_placeholder: y})
                #     sess.run([next_x, next_y])

                tf.identity(next_x[0], 'enc_0')
                tf.identity(next_y[0], 'dec_0')

                # Set runhook to initialize iterator
                iterator_initializer_hook.iterator_initializer_func = \
                    lambda sess: sess.run(
                        iterator.initializer,
                        feed_dict={input_placeholder: x,
                                   output_placeholder: y})
                                   
                # Return batched (features, labels)
                return next_x, next_y

        # Return function and hook
        return train_inputs, iterator_initializer_hook

    return get_inputs()
