from __future__ import print_function
from .config import Config
import nltk
import tensorflow as tf

from . import model

class Sarika:

    def __init__(self):
        pass

    def model_fn(self, mode, features, labels, params):
        self.dtype = tf.float32
        self.mode = mode

        self.loss, self.train_op, self.metrics, self.predictions = None, None, None, None
        self._init_placeholder(features, labels)
        self.build_graph()

        # train mode: required loss and train_op
        # eval mode: required loss
        # predict mode: required predictions
        
        return tf.estimator.EstimatorSpec(
            mode=mode,
            loss=self.loss,
            train_op=self.train_op,
            eval_metric_ops=self.metrics,
            predictions={"prediction": self.predictions})

    def _init_placeholder(self, features, labels):
        self.encoder_inputs = features
        if type(features) == dict:
            self.encoder_inputs = features["input_data"]

        batch_size = tf.shape(self.encoder_inputs)[0]

        if self.mode == tf.estimator.ModeKeys.TRAIN or self.mode == tf.estimator.ModeKeys.EVAL:
            self.decoder_inputs = labels
            decoder_input_shift_1 = tf.slice(self.decoder_inputs, [0, 1],
                    [batch_size, int(Config.get('data')['max_seq_length'])-1])
            pad_tokens = tf.zeros([batch_size, 1], dtype=tf.int32)

            # make target (right shift 1 from decoder_inputs)
            self.targets = tf.concat([decoder_input_shift_1, pad_tokens], axis=1)
        else:
            self.decoder_inputs = None

    def build_graph(self):
        graph = model.Graph(mode=self.mode)
        graph.build(encoder_inputs=self.encoder_inputs,
                    decoder_inputs=self.decoder_inputs)

        self.predictions = graph.predictions
        if self.mode != tf.estimator.ModeKeys.PREDICT:
            self._build_loss(graph.logits, graph.weight_masks)
            self._build_optimizer()
            self._build_metric()
    
    def learning_rate(self, train_step):
        if train_step <= 1.48:
            return 9.6e-5
        elif train_step <= 1.64:
            return 1e-4
        elif train_step <= 2.0:
            return 1.2e-4
        elif train_step <= 2.4:
            return 1.6e-4
        elif train_step <= 3.2:
            return 2e-4
        elif train_step <= 4.8:
            return 2.4e-4
        elif train_step <= 8.0:
            return 3.2e-4
        elif train_step <= 16.0:
            return 4e-4
        elif train_step <= 32.0:
            return 6e-4
        else:
            return 8e-4
    
    def _build_loss(self, logits, weight_masks):
        self.loss = tf.contrib.seq2seq.sequence_loss(
                logits=logits,
                targets=self.targets,
                weights=weight_masks,
                name="loss")
#         self.loss = tf.nn.sparse_softmax_cross_entropy_with_logits(
#             labels=self.targets, logits=logits)

        self.cost = tf.reduce_mean(self.loss)        

    def _build_optimizer(self):
        self.learning_rate = float(Config.get("train")["learning_rate"])
#         self.train_op = tf.contrib.layers.optimize_loss(
#             self.loss, tf.train.get_global_step(),
#             optimizer='Adam',
#             learning_rate=float(Config.get('train')['learning_rate']),
#             summaries=['loss', 'learning_rate'],
#             name="train_op")
        tvars = tf.trainable_variables() # tvars is a python list of all trainable TF Variable objects.
            # tf.gradients returns a list of tensors of length len(tvars) where each tensor is sum(dy/dx).
        grads, _ = tf.clip_by_global_norm(tf.gradients(self.cost, tvars), int(Config.get('model')['grad_clip']) )
        
        # gradient_norm_summary = [tf.summary.scalar("grad_norm", grads)]
        # gradient_norm_summary.append(
        #     tf.summary.scalar("clipped_gradient", tf.global_norm(grads))
        # )
        gradient_norm_summary = [tf.summary.scalar("clipped_gradient", tf.global_norm(grads))]
#         global_step = tf.Variable(0, name='global_step', trainable=False)
#         starter_learning_rate = 0.1
#         learning_rate = tf.train.exponential_decay(starter_learning_rate, global_step,
#                                            100000, 0.96, staircase=True)

        optimizer = tf.train.AdamOptimizer(self.learning_rate) # Use ADAM optimizer.
      
        self.train_op = optimizer.apply_gradients(zip(grads, tvars), global_step=tf.train.get_global_step() )
            #self.train_op = optimizer.minimize(self.cost)
        # Summary
        self.train_summary = tf.summary.merge([
                tf.summary.scalar("learning_rate", self.learning_rate),
                tf.summary.scalar("loss", self.loss),
                tf.summary.scalar("cost", self.cost),
        ] + gradient_norm_summary)

    def _build_metric(self):

        def blue_score(labels, predictions,
                       weights=None, metrics_collections=None,
                       updates_collections=None, name=None):

            def _nltk_blue_score(labels, predictions):

                # slice after <eos>
                predictions = predictions.tolist()
                for i in range(len(predictions)):
                    prediction = predictions[i]
                    print("prediction", prediction)
                    if int(Config.get('data')['EOS_ID']) in prediction:
                        predictions[i] = prediction[:prediction.index(int(Config.get('data')['EOS_ID']))+1]

                labels = [
                    [[w_id for w_id in label if w_id != int(Config.get('data')['PAD_ID'])]]
                    for label in labels.tolist()]
                predictions = [
                    [w_id for w_id in prediction]
                    for prediction in predictions]

                return float(nltk.translate.bleu_score.corpus_bleu(labels, predictions))

            score = tf.py_func(_nltk_blue_score, (labels, predictions), tf.float64)
            return tf.metrics.mean(score * 100)

        self.metrics = {
            "accuracy": tf.metrics.accuracy(self.targets, self.predictions),
            "bleu": blue_score(self.targets, self.predictions)
        }
