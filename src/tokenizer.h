#ifndef SARIKA_TOKENIZER_H
#define SARIKA_TOKENIZER_H
#include <string>
#include <map>
#include <vector>

namespace sarika {



enum PROC_TYPE{
    DEC,
    ENC
};


class Tokenizer {

    public:
        Tokenizer(std::map<std::string, std::string> config);
        void Train();
        void Test();
        std::vector<std::string> GetVocabs();
        std::vector<std::string> Sentence2Id(std::string line);
        void ToId(std::string name, PROC_TYPE type);
    private:
        void Vocab2Id();
        std::map<std::string, std::string> config;
        int src_max_len;
        int tgt_max_len;
        long vocab_size;
        std::vector<std::string> vocabs;
        std::map<std::string, int> vocabIds;
};
}

#endif