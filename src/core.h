#include <vector>
#include <iostream>

template<typename T>
inline std::ostream& operator<< (std::ostream& out, const std::vector<T>& v) {
    out << "{";
    size_t last = v.size() - 1;
    for(size_t i = 0; i < v.size(); ++i) {
        out << v[i];
        if (i != last) 
            out << ", ";
    }
    out << "}";
    return out;
}

// template <class T>
// std::vector<T>& operator+=(std::vector<T>& lhs, T *l)
// {   
//     for ( int i = 0; i < sizeof(l)/sizeof(l[0]); ++i ){
//         std::cout << l[i] << std::endl;
//         lhs.push_back(l[i]);
//     }
//     return lhs;
// }

template <typename T>
inline std::vector<T> &operator+=(std::vector<T> &A, const std::vector<T> &B)
{
    A.reserve( A.size() + B.size() );                // preallocate memory without erase original data
    A.insert( A.end(), B.begin(), B.end() );         // add B;
    return A;                                        // here A could be named AB
}