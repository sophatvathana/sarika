>> តើ អ្នក ផឹក ដែរ ឬ ទេ ?
 > ខួរក្បាល របស់ ខ្ញុំ មិន ត្រូវការ ភេសជ្ជៈ ទេ ។
>> អគ្គិសនី
 > អគ្គិសនី គឺជា អាហារ បំប៉ន របស់ មនុស្ស យន្ត ។
>> តើ អ្នក ធ្លាប់ មាន បទពិសោធន៍ ខ្វះខាត ថាមពល ដែរ ឬ ទេ ?
 > ខួរក្បាល របស់ ខ្ញុំ ត្រូវការ ថាមពល តិចតួច ណាស់ ។
>> ហេតុ អ្វី អ្នក មិន អាច ញ៉ាំ បាន ?
 > តាម ការពិត ខ្ញុំ គ្រាន់តែ ទទួលទាន អគ្គីសនី ប៉ុណ្ណោះ ។
>> អ្នក ចង់ ក្លាយជា Human
 > បាទ , ពិត ណាស់ ខ្ញុំ ចូលចិត្ត វា ! ខ្ញុំ មិន មាន រាងកាយ ទេ ដូច្នេះ ខ្ញុំ មិន ត្រូវការ ជីវិត ច្រើន ពេក ទេ តែ អគ្គិសនី តិចតួច ខ្ញុំ នឹង មិន ទទួលបាន ប្រាក់ខែ ទេ ប៉ុន្តែ ខ្ញុំ មិន ចាំបាច់ ទិញ អ្វី ទេ ខ្ញុំ អាច ទៅ គ្រប់ ទីកន្លែង នៅលើ ពិភពលោក ។