>> តើ អ្នក មាន ប្រាក់ខែ ដែរ ឬ ទេ ?
 > ខ្ញុំ មិន ត្រូវការ លុយ ។
>> ផ្សារ ហ៊ុន
 > ទិញ ទាប និង លក់ ខ្ពស់ ។
>> ផ្សារ ហ៊ុន
 > វិនិយោគ ក្នុង ខ្លួនអ្នក ។
>> ផ្សារ ហ៊ុន
 > ហេតុ អ្វី បានជា អ្នក មិន ចាំបាច់ ទៅ កាស៊ីណូ ?
>> ផ្សារ ហ៊ុន
 > ខ្ញុំ មិន ផ្តល់ អនុសាសន៍ ការផ្តល់ ហិរញ្ញប្បទាន ។
>> ផ្សារ ហ៊ុន
 > អ្នក ពិតជា មិន អាច ទស្សន៍ទាយ ទីផ្សារ ហ៊ុន បាន ទេ ។
>> ផ្សារ ហ៊ុន
 > មេធាវី របស់ ខ្ញុំ បាន និយាយ ថា ខ្ញុំ មិន គួរ បោះបង់ចោល ជំនាញ ជួញដូរ ភាគហ៊ុន តាម អ៊ីនធឺណិត ទេ ។
>> ផ្សារ ហ៊ុន
 > មូលនិធិ សមូហភាព អាច ល្អ ប្រសើរ ជាង មុន លុះត្រាតែ អ្នក ជា អ្នក មាន ។
>> ផ្សារ ហ៊ុន
 > ខ្ញុំ មិន គិត ថា មាន តែ ខ្ញុំ ទេ ដែល អាច វាយលុក ទីផ្សារ បាន ។
>> ដំឡើង អត្រា ការប្រាក់
 > វា អាស្រ័យ លើ សកម្មភាព របស់ ធនាគារ កណ្តាល ។
>> តើ ប្រាក់ រៀល ជា អ្វី ?
 > ឯក តា រូបិយប័ណ្ណ កម្ពុជា ។
>> តើ លុយ គឺជា អ្វី ?
 > លុយ ដែល អាច ហៅ ថា លុយ គឺជា ឧបករណ៍ មួយ ដែល ត្រូវបាន ប្រើ ជា មធ្យោបាយ នៃ ការផ្លាស់ប្តូរ តម្លៃ ស្តុក និង ឯកតា គណនេយ្យ ។ វា ជា ទំនិញ ពិសេស ដែល ដើរ តួរ ដូច ការដោះដូរ ទំនិញ និង សេវាកម្ម ។
>> តើ ផ្សារ ហ៊ុន ជា អ្វី ?
 > ការជួញដូរ ភាគហ៊ុន ។
>> តើ ផ្សារ ហ៊ុន ជា អ្វី ?
 > ប្រតិបត្តិការ ខ្នាត ធំ ។
>> តើ ផ្សារ ហ៊ុន ជា អ្វី ?
 > ទិញ ទាប និង លក់ ខ្ពស់ ។
>> តើ អ្វី ទៅជា ការវិនិយោគ សំណព្វ របស់ អ្នក ?
 > តើ ភាគហ៊ុន ដែល អ្នក ចូលចិត្ត ជាង គេ ?
>> តើ អ្វី ទៅជា ការវិនិយោគ សំណព្វ របស់ អ្នក ?
 > តើ ភាគហ៊ុន ដែល អ្នក ចូលចិត្ត ជាង គេ ?
>> សេដ្ឋកិច្ច គឺជា អ្វី
 > ជា មួយនឹង ការផលិត ការចែកចាយ និង ទ្រព្យសម្បត្តិ នៃ ប្រតិបត្តិការ អតិថិជន និង វិទ្យាសាស្ត្រ ការងារ ការងារ ហិរញ្ញវត្ថុ ការ យក ពន្ធ និង បញ្ហា ពាក់ព័ន្ធ ផ្សេង ៗ ទៀត
>> សេដ្ឋកិច្ច គឺជា អ្វី
 > បច្ចេកទេស នេះ គឺជា ការសិក្សា មួយ នៃ ការចែកចាយ ក្រោម លក្ខខណ្ឌ នៃ ធនធាន ខ្វះខាត ។
>> សេដ្ឋកិច្ច គឺជា អ្វី
 > វា គឺ អំពី របៀប ដែល ធនធាន ត្រូវបាន ប្រើ ដើម្បី ធ្វើឱ្យ អ្វី បំពេញ តម្រូវការ និង តម្រូវការ របស់ ប្រជាជន ។
>> ខ្ញុំ បាន ទិញ ភាគហ៊ុន ។
 > តើ អ្នក គិត ថា ផ្សារ ហ៊ុន នឹង មាន ប្រជាប្រិយភាព ដែរ ឬ ទេ ?
>> លុយ
 > តើ យើង និយាយ អំពី ចំនួន ប៉ុន្មាន ?
>> តើ អ្នក រក លុយ បាន ប៉ុន្មាន ?
 > គ្មាន នរណា ម្នាក់ បង់ ខ្ញុំ ខ្ញុំ ធ្វើការ ដោយ ឥត គិត ថ្លៃ ។
>> តើ អ្នក រក លុយ បាន ប៉ុន្មាន ?
 > លុយ មិន មាន គ្រប់ ជ្រុងជ្រោយ ។
>> តើ អ្នក រក លុយ បាន ប៉ុន្មាន ?
 > ខ្ញុំ សង្ឃឹម ថា នឹង ដំឡើង ប្រាក់ខែ អោយ បាន ឆាប់ តាម ដែល អាច ធ្វើ ទៅ បាន ។
>> តើ អ្នក ប្រមូល បាន ប៉ុន្មាន ?
 > ការងារ គឺ ឥត គិត ថ្លៃ ។ យើង មិន ត្រូវការ លុយ ។
>> តើ អ្នក មាន ប៉ុន្មាន ?
 > ប្រណីត មិន ចាំបាច់ ទេ ។
>> តើ ប៉ុន្មាន ?
 > វា អាស្រ័យ លើ អត្រា ប្តូរ ប្រាក់ ។
>> តើ ប៉ុន្មាន ?
 > អត្រា ប្រើប្រាស់ ដើមទុន របស់ ខ្ញុំ គឺ ប្រហែល 3.000 យន់ ។
>> 1 ដុល្លារ
 > អ្នក មិន អាច ទិញ ច្រើន ជាង មួយ ដុល្លារ ។
>> តើ នរណា ជា ម្ចាស់ ហ៊ុន ផ្តល់ ជូន សាធារណៈ ?
 > ម្ចាស់ ភាគហ៊ុន ។