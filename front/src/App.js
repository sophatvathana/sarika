import React, { Component } from 'react';
import { ChatFeed, Message } from 'react-chat-ui'
import send_icon from './resources/send_icon.svg';
import right_arrow from './resources/right-arrow.svg';
import unsure from './resources/unsure.ogg';
import receive_sound from './resources/to-the-point.ogg';
import sarika from './sarika.png';
import IO from 'socket.io-client'
import * as Scroll from 'react-scroll';
import { Link, Element , Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll'
import './App.css';
import ReactHowler from 'react-howler'

const socket = IO.connect('http://35.194.149.160:3010')
class App extends Component {

  constructor(props) {
    super(props)
    this.myRef = React.createRef() // create a ref object 
    this.state = {
      textMessage: '',
      playing: false,
      stop:false,
      rc_playing: false,
      rc_stop:false,
      messages: [
        new Message({ id: 1, message: "សូមសួរខ្ញុំ!" }),  
      ],
      Element:Scroll.Element,
    };
}
  componentDidMount(){
    socket.on('message', (e)=>{
      const returnMessage = new Message({id: 1, message: e});
      this.handleAddNewMessage(returnMessage)
      this.setState({is_typing:false, rc_playing:true})
      setTimeout(()=>{
        this.setState({rc_stop:true})
      }, 1000)
    })
  }

  handleMessageSocket (message, callback) {
    socket.emit('message', message, callback)
  }
 
  onSend = () => {
    this.handleSend()
  }

  onValueChange = (evt) => {
    this.setState({
      textMessage: evt.target.value
    })
  }

  handleAddNewMessage (message) {
    const msgList=this.state.messages;
    const newMsgList = [...msgList, message]
    this.setState({
      messages: newMsgList
    })
  }

  handleSend(){
    const myMessage= new Message({id: 0, message: this.state.textMessage});
    this.handleAddNewMessage(myMessage)
    this.setState({
      textMessage: ''
    });
    this.handleMessageSocket(this.state.textMessage, value=> {
    })
    this.scrollToBottom();
  }
  scrollToBottom() {
    scroll.scrollToBottom();
  };
  onKeyPress = (evt) => {
    if (evt.key === 'Enter') {
      if(this.state.textMessage == '')
        return
      this.handleSend() 
      this.setState({is_typing:true, playing: true})
      setTimeout(()=>{
        this.setState({playing: false, stop:true})
      }, 1000)
     
    }
  }
  
  render() {
    const customBubble = props => {
      const style = {
        backgroundColor: props.message.id == 0?'#363334':'rgb(240, 137, 35)', 
        borderRadius: '10px', 
        margin: '1px 15px', 
        maxWidth: '425px', 
        padding: '10px', 
        width: '-webkit-fit-content', 
        float: props.message.id == 0 ?'right':'left', 
        textAlign: 'left'
      }
      return (
        <div className={props.message.id == 0?'right-bubble':'left-bubble'} style={{marginTop: '10px', marginBottom: '10px', overflow: 'auto', position: 'relative'}}>
          <div style={{overflow: 'auto'}}>
            <div style={style}>
              <p style={{color: 'rgb(255, 255, 255)', fontSize: '15px', fontWeight: 300, margin: '0px', fontFamily: 'Bayon'}}>{props.message.message}
              </p>
            </div>
          </div>
        </div>
    )};
    const style = {
      backgroundColor: 'rgb(240, 137, 35)', 
      borderRadius: '10px', 
      margin: '3px 4px',
      textAlign: 'center',
      height: '35px'
    }
    return (
      <div className="App" style={{flex:1}} >
         <ReactHowler
            src={[unsure]}
            playing={this.state.playing}
            stop={this.state.stop}
            display={false}
          />
          <ReactHowler
            src={[receive_sound]}
            playing={this.state.rc_playing}
            stop={this.state.rc_stop}
            display={false}
          />
          
        <div style={{flex:1,flexDirection:'row' ,display: 'flex',backgroundColor:'#f0f3f6',justifyContent:'space-between', borderBottom:'1px solid #ccc', position:'fixed', width:'100%', zIndex: 999}}>

          <div style={{flex:1, display:'flex', justifyContent:'flex-start'}}>
            <img src={sarika} style={{marginLeft: '1em'}} alt="Smiley face" height="42" width="42"></img>
            <div style={style} className="sarika-logo">
              <p style={{color: 'rgb(255, 255, 255)', fontSize: '15px', fontWeight: 300,fontFamily: 'Bayon', margin: 0, lineHeight: '2em',padding: '4px', width:'60px'}}> សាកល្បង
              </p>
            </div>
          </div>
          <div style={{paddingTop: '8px',display:'flex'}}>
            <div style={{marginRight: '1em'}}><a class="github-button" href="https://github.com/slashdigital/khmer-chatbot-up2018" data-size="large" data-show-count="true" aria-label="Star slashdigital/khmer-chatbot-up2018 on GitHub">Star</a></div>
            <div style={{marginRight: '1em'}}><a class="github-button" href="https://github.com/slashdigital/khmer-chatbot-up2018/fork" data-size="large" data-show-count="true" aria-label="Fork slashdigital/khmer-chatbot-up2018 on GitHub">Fork</a></div>
          </div>
        </div>
        <div style={{top: '2em', border: '1px solid #ccc',width:'30%', margin: '0 auto', position:'relative', minHeight: '100vh', borderTop: 'none', backgroundImage: 'url(https://i.pinimg.com/564x/8f/ba/cb/8fbacbd464e996966eb9d4a6b7a9c21e.jpg)'}} >  
              <div style={{marginBottom:50}}>
                  <ChatFeed
                      chatBubble={customBubble}
                      messages={this.state.messages} // Boolean: list of message objects
                      isTyping={this.state.is_typing} // Boolean: is the recipient typing
                      hasInputField={false} // Boolean: use our input, or use your own
                      showSenderName={false} // show the name of the user who sent the message
                      bubblesCentered={true} //Boolean should the bubbles be centered in the feed?
                      // JSON: Custom bubble styles
                      bubbleStyles={{ text: { fontSize: 15,  fontFamily: 'Bayon' },
                      chatbubble: { textAlign: 'left', borderRadius: 10, padding: 10, marginLeft:15, marginRight:15}}}
                  />
              </div>
              <div style={{flex:1, flexDirection: 'row', display:'flex', width:'30%', margin: '0 auto', position: 'fixed', bottom: 0, left: 0, right: 0, border: '1px solid #ccc', background: '#fff'}}>
                  <div style={{flex:1}}> 
                    <MessageInput
                        onKeyPress={this.onKeyPress}
                        message={this.state.textMessage}
                        onValueChange={this.onValueChange}
                    />
                  </div>
                  <button style={{backgroundColor:'transparent', borderColor:'tranparent', borderWidth:0}} 
                      onClick={this.onSend}> 
                      <img style={{width:'28px'}} src={right_arrow} alt="send"/>
                  </button>
              </div>
          </div>
      </div>
    );
  }
}

const MessageInput = ({message, onValueChange, onKeyPress})=>(
    <input
      style={
        {width: '100%', 
        height: 40, 
        paddingLeft: '8px',
        fontSize: 15, 
        border: '0 !important', 
        boxShadow: '0 !important',
        outline: 'none',
        fontFamily: 'Bayon',
        border: 'none'
      }}
      onChange={onValueChange}
      onKeyPress={onKeyPress}
      value={message}
      placeholder="បញ្ចូលសាររបស់អ្នក ហើយវាយបញ្ចូល!"
      type="text" />
  )

export default App;
