const server = require('http').createServer()
const io = require('socket.io')(server)
const axios = require('axios')

io.on('connection', function (client) {
  

  client.on('message', handleMessage)


  client.on('disconnect', function () {
    console.log('client disconnect...', client.id)
    handleDisconnect()
  })

  client.on('error', function (err) {
    console.log('received error from client:', client.id)
    console.log(err)
  })
})

server.listen(3010, function (err) {
  if (err) throw err
  console.log('listening on port 3010')
})


const handleMessage = (message, callback) => {
    
    if(callback) {
      axios.post('http://localhost:8000/chat', {
        q: message
      })
      .then(function (response) {
        callback(response)
      })
      .catch(function (error) {
        console.log(error);
      });

    }
    
}
const handleDisconnect = () => {
    console.log('handle disconnect')
}